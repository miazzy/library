/**
 * @function 加载JS/CSS脚本
 * @param filename 待加载脚本名称
 */
function loadJS(filename) {
    var fileref = document.createElement('script');
    fileref.setAttribute("type", "text/javascript");
    fileref.setAttribute("src", filename);
    document.getElementsByTagName("head")[0].appendChild(fileref);
}

/**
 * @function 为每行附件框设置展示图片
 */
function setupImage(name, wTableID, mainKeyID) {

    //捕获异常，避免异常后JS停止执行
    try {

        //记录此函数遍历次数
        if (window.scount == null || window.scount == undefined) {
            window.scount = 0;
        }

        //获取附件上传按钮的个数
        var isLoad = $("#grid_" + wTableID + "grid").find('.l-grid-row-cell a').length;

        //如果附件上传按钮的个数大于0，则进行设置图片展示功能
        if (isLoad != null && isLoad != undefined && isLoad > 0) {

            //设置行记录高度
            $("#grid_" + wTableID + "grid").find('.l-grid-row-cell').css('height', '120');

            if (name == "all") {
                $("#grid_" + wTableID + "grid").find('.l-grid-row-cell a').each(function() {
                    changeHtml(this, mainKeyID);
                });
            } else {
                //遍历每行记录的附件框，并设置图片展示
                $("#grid_" + wTableID + "grid td[id$='" + name + "']").each(function() {

                    //获取对应HTML标签下面的A标签
                    var item = $(this).find('a');

                    //改变对应附件框的HTML的内容
                    changeHtml(item, mainKeyID);

                });
            }

        } else {

            //为避免执行次数过多，设置最多3次设置展示图片操作
            if (window.scount++ <= 0) {
                //问题清单显示后，设置展示图片
                setTimeout(function() {
                    //设置展示图片
                    setupImage(name);
                }, 1500 * window.scount);
            }

        }

    } catch (e) {

    }
}

/**
 * @function 修改对应输入框HTML内容
 * @param selector 
 */
function changeHtml(selector, mainKeyID) {

    var href = $(selector).attr('href');
    var info = "";
    var url = "/cwbase/web/session/GSIDP/GSYS/WEB/GSYDownLoadFile.aspx?id={GSYFILESAVE_ID}";
    var fieldName = mainKeyID.split('_')[0] + '_';

    if (href != null && href != undefined) {

        //获取对应清单记录信息
        vscript = href.toString();
        href = href.replace("javascript:openGridFile(", "");
        href = href.substring(0, href.length - 1);
        info = JSON.parse("[" + href + "]");
        fieldName = fieldName + $(selector).parent().parent().parent().attr('id').split(fieldName)[1];

        //查询对应记录的图片信息
        var pictures = DealAjax.getJsonDataTable(dbSaveSrc,
            'GSYFILESAVE',
            'GSYFILESAVE_ID , GSYFILESAVE_PATH , GSYFILESAVE_FILENAME',
            " AND GSYFILESAVE_REFKEY = '" + info[0][mainKeyID] + "' AND GSYFILESAVE_FIELDID = '" + fieldName + "' AND GSYFILESAVE_EXT IN ('.jpg' , '.JPG' , '.jpeg' ,  '.JPEG' ,  '.png' , '.PNG' , '.bmp' , '.BMP' , '.tif' ,  '.TIF' , '.ico' , '.ICO' , '.gif' , '.GIF' ) ");

        if (pictures && pictures.Rows && pictures.Rows.length > 0) {

            var details = '';

            //遍历数组
            pictures.Rows.forEach(function(item) {
                details = details + ',' + item['GSYFILESAVE_ID'];
            });

            var iurl = url.replace("{GSYFILESAVE_ID}", pictures.Rows[0]['GSYFILESAVE_ID']);
            $(selector).parent().parent().parent().html("<a href='javascript:viewFile(\"" + pictures.Rows[0]['GSYFILESAVE_ID'] + "\" , \"" + fieldName + "\" , \"" + details + "\" )' >" + '<img  style="max-height:115px;max-width:181px;" src="' + iurl + '" alt=""></a>');
        }
    }
}

/**
 * @function 查看附件Dialog
 * @param id 图片GUID 
 */
function viewFile(id, fieldID, details) {

    var urlview = "/cwbase/web/session/GSIDP/GSYS/WEB/GSYDocOfficeCtrl.aspx?FileName=&EXT=&id=" + id + "&MKID=PMQM&FIELDID=" + fieldID + "&filepath=";
    var winwidth = $(window).width() - 100;
    var winheight = $(window).height() - 80;

    //打开对应图片Dialog窗口
    window.$.ligerDialog.open({
        title: "查看附件",
        name: 'viewdoc',
        isHidden: false,
        showMax: true,
        width: winwidth,
        height: winheight,
        url: urlview
    });

    //等待Dialog窗口打开完毕后，执行删除多余图片操作
    setTimeout(function() {

        //页面加载完毕后，需要去掉不需要的图片
        $("#viewdoc").contents().find('.listView').each(function() {

            //获取对应图片ID
            var id = $(this).attr('dsid');

            //如果views中不存在对应的ID值，则删除相应的图片
            if (details.indexOf(id) < 0) {
                $(this).remove();
            }
        });

    }, 1500);

    return false;
}