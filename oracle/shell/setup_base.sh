#!/bin/bash                                                                                                                                                                     
                                                                                                                                                                                
set -e                                                                                                                                                                          
                                                                                                                                                                                
export DBCA_TOTAL_MEMORY=1024                                                                                                                                                   
export WEB_CONSOLE="true"                                                                                                                                                       
export ORACLE_SID=BRCGS                                                                                                                                                            
export ORACLE_HOME=/u01/app/oracle/product/11.2.0/BRCGS                                                                                                                            
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:$ORACLE_HOME/bin                                                                                       
                                                                                                                                                                                
#Reset mirrors                                                                                                                                                                  
#wget -O /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-7.repo                                                                                         
                                                                                                                                                                                
#Download RPM KEY                                                                                                                                                               
cd /etc/yum.repos.d                                                                                                                                                             
#wget http://yum.oracle.com/public-yum-ol7.repo                                                                                                                                 
#wget http://public-yum.oracle.com/RPM-GPG-KEY-oracle-ol7 -O /etc/pki/rpm-gpg/RPM-GPG-KEY-oracle                                                                                
                                                                                                                                                                                
                                                                                                                                                                                
groupadd dba && useradd -m -G dba oracle && mkdir /u01 && chown oracle:dba /u01                                                                                                 
yum install -y oracle-rdbms-server-11gR2-preinstall glibc-static wget unzip                                                                                    
                                                                                                                                                                                
#Delete limits cause of docker limits issue                                                                                                                                     
cat /etc/security/limits.conf | grep -v oracle | tee /etc/security/limits.conf                                                                                                  
                                                                                                                                                                                                                                                                                                          
echo 'Unzipping'                                                                                                                                                                
cd /install                                                                                                                                                                     
unzip -q linux.x64_11gR2_database_1of2.zip                                                                                                                                      
unzip -q linux.x64_11gR2_database_2of2.zip                                                                                                                                      
#rm -f linux*.zip                                                                                                                                                               
                                                                                                                                                                                
mv database /home/oracle/                                                                                                                                                       
                                                                                                                                                                                
su oracle -c 'cd /home/oracle/database && ./runInstaller -ignorePrereq -ignoreSysPrereqs -silent -responseFile /install/oracle-11g-ee.rsp -waitForCompletion 2>&1'              
rm -rf /home/oracle/database                                                                                                                                                    
                                                                                                                                                                                
mv /u01/app/oracle/product /u01/app/oracle-product 